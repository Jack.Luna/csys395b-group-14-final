#!/usr/bin/env python
# coding: utf-8

# # Group 14 Code 
# 

# To run this file, just ensure that the file "mbti_1.csv" is in the same directory. To use the live dataset from the Kaggle page, remove the headers manually from the file and rename it. Required libraries are SkLearn and Tensorflow.

# ## Imports
# 

# In[ ]:




import re
import copy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn as sk
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import f1_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import OneHotEncoder
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential 
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras import optimizers
from tensorflow.keras.preprocessing.text import one_hot


# ## Preprocessing
# 

# ### Sterilization and formatting

# In[ ]:


#Dictionary for translating text labels into numerical labels
mbti_dict = {
    "ISTJ": 0,
    "ISFJ": 1,
    "INFJ": 2,
    "INTJ": 3,
    "ISTP": 4,
    "ISFP": 5,
    "INFP": 6,
    "INTP": 7,
    "ESTP": 8,
    "ESFP": 9,
    "ENFP": 10,
    "ENTP": 11,
    "ESTJ": 12,
    "ESFJ": 13,
    "ENFJ": 14,
    "ENTJ": 15
}


# In[ ]:


#Read in csv data
mbti_file = open("mbti_1.csv", "r", encoding="utf-8")
mbti_rows = mbti_file.readlines()
mbti_data_list = []


# In[ ]:


#Split the csv into label and data
for i in range(len(mbti_rows)):
    mbti_data_list.append([mbti_rows[i][0:4],mbti_rows[i][5:]])


# In[ ]:


#Process posts from raw into lowercase words with no special characters
#Seperated by one exactly one space.

mbti_list_processed = copy.deepcopy(mbti_data_list)

alpha_match = re.compile('[^a-zA-Z \']')#Matches special characters for removal
link_match = re.compile("\\b[^\s]*(http)[^\s]+\\b")#Matches links for removal

multispace_match = re.compile(r" +")#Matches groups of more than one space for removal

for j in range(len(mbti_list_processed)): #Apply all of the filters to the data
    posts_raw = mbti_list_processed[j][1]
    posts = posts_raw.casefold()
    posts = posts.replace('|||', ' ')#Remove delimiter characters between posts
    
    posts = " "+posts+" "
    posts = link_match.sub(' ', posts)
    posts = alpha_match.sub(' ', posts)
    posts = posts.replace("'",'')#Remove apostrophes before spaces to avoid a bug with contractions registering as two words
    posts = multispace_match.sub(' ', posts)
    mbti_list_processed[j][1] = posts;
    


# In[ ]:


#Swap rows and columns for ease of use

mbti_zipped = list(zip(*mbti_list_processed)) #Swap rows, columns.


# In[ ]:


#Convert from text targets to numbered targets

target_nums = []

for t in mbti_zipped[0]:
    target_nums.append(mbti_dict[t]) 
    
print(len(target_nums))


# ### Vectorization (tf-idf)

# In[ ]:


#Run TfIdf Vectorization
tf_idf_vect = TfidfVectorizer(use_idf=True, 
                        smooth_idf=True,  
                        ngram_range=(1,1),stop_words='english', min_df=.05)


# In[ ]:


#transform and format
tf_idf_data = tf_idf_vect.fit_transform(mbti_zipped[1])
tf_arr = tf_idf_data.toarray()
target_num_arr = np.array(target_nums)
target_num_arr = np.reshape(target_num_arr, (target_num_arr.shape[0],1))


# In[ ]:


#Scale the data
data_scaler = StandardScaler()
tf_arr = data_scaler.fit_transform(tf_arr)


# #### PCA to 500 dimensions! Only run this cell if using MLP

# In[ ]:


#PCA
#===============================
pca = PCA(n_components = 500)
tf_arr = pca.fit_transform(tf_arr)
#===============================



print(tf_arr.shape)


#    

# In[ ]:


#Attach the target to the data
full_data = np.append(tf_arr, target_num_arr, 1)
print(full_data.shape)


# In[ ]:


#Shuffle
np.random.shuffle(full_data)


# In[ ]:


#Train test split
x_train, x_test, y_train, y_test = train_test_split(full_data[:, :-1], full_data[:, -1], test_size = .5, random_state = 0)


# In[ ]:


#Check distribution of test values
hist = [0] * 17
for num in y_test:
    hist[int(num)] += 1
    
print(hist)
hist_balanced = [0]*16
for i in range(len(hist)-1):
    hist_balanced[i] = hist[i+1]
    
print(hist_balanced)
print(np.average(hist_balanced))
    
fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
cats = ["ISTJ",
    "ISFJ",
    "INFJ",
    "INTJ",
    "ISTP",
    "ISFP",
    "INFP",
    "INTP",
    "ESTP",
    "ESFP",
    "ENFP",
    "ENTP",
    "ESTJ",
    "ESFJ",
    "ENFJ",
    "ENTJ"]
ax.bar(cats,hist_balanced)
plt.show()


# # Model Training/Testing

# ## Multi-layer perceptron (REQUIRES PCA ABOVE)

# ### Training

# In[ ]:


#Convert to categorical data (one-hot)
np.random.shuffle(full_data)
x_train, x_test, y_train, y_test = train_test_split(full_data[:, :-1], full_data[:, -1], test_size = .5, random_state = 0)
y_train = to_categorical(y_train, num_classes=16)
y_test = to_categorical(y_test, num_classes=16)


# In[ ]:


#Define model
model = Sequential()
model.add(Dense(100, input_dim=500, activation='relu'))
model.add(Dense(1000, activation='relu'))
model.add(Dense(16, activation='softmax'))


# In[ ]:


#Compile and fit
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
history = model.fit(x_train, y_train, epochs=250, batch_size=500)


# ### Testing

# In[ ]:


#Predict!
y_pred = model.predict(x_test)
#Converting predictions back to label
pred = list()
for i in range(len(y_pred)):
    pred.append(np.argmax(y_pred[i]))
#Converting one hot encoded test label to label
test = list()
for i in range(len(y_test)):
    test.append(np.argmax(y_test[i]))


# In[ ]:


#Report results
print(classification_report(pred,test))


# In[ ]:


#Confusion matrix
mlp_conf = confusion_matrix(test, pred)
print(mlp_conf)


# In[ ]:


#Show confusion matrix
plt.imshow(mlp_conf, cmap='cool')


# ## Random Forest (NO PCA)

# ### Training

# In[ ]:


RFmodel = RandomForestClassifier(n_estimators = 1000, warm_start = True)
RFmodel.fit(x_train, y_train)


# ### Testing

# In[ ]:


accuracyRF = RFmodel.score(x_test, y_test)
print(accuracyRF)
RFpreds = RFmodel.predict(x_test)
RFconf = confusion_matrix(y_test, RFpreds)
print(RFconf)


# In[ ]:


#Print Results
print(classification_report(y_test, RFpreds))


# ## Logistic Regression

# ### Training

# In[ ]:


reg_1 = LogisticRegression(max_iter = 100000)
reg_1.fit(x_train,y_train)


# ### Testing

# In[ ]:


reg_1_preds = reg_1.predict(x_test)
reg_conf = confusion_matrix(y_test, reg_1_preds)
print(reg_conf)


# In[ ]:


#Print Classification
print(classification_report(y_test, reg_1_preds))

